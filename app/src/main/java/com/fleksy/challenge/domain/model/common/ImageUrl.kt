package com.fleksy.challenge.domain.model.common

import com.fleksy.challenge.ConstAndValues

data class ImageUrl(private var url: String?) {
    init {
        if (url == null) url = ""
    }

    operator fun get(type: ImageType): String {
        return ConstAndValues.baseImageUrl + type.value + url
    }
}

enum class ImageType(val value: String) {
    BACKGROUND("w780"),
    LOGO("w92"),
    POSTER("w342"),
    PROFILE("w185")
}