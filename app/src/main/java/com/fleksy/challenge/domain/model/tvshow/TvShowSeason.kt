package com.fleksy.challenge.domain.model.tvshow

import com.fleksy.challenge.domain.model.common.ImageUrl

data class TvShowSeason(
    val id: Int,
    val name: String,
    val image: ImageUrl,
    val number: Int?,
    val episodeCount: Int?,
    val airDate: String?
)
