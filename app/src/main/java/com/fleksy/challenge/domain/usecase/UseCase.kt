package com.fleksy.challenge.domain.common

import kotlinx.coroutines.*

abstract class UseCase<Params, Result> {
    protected abstract suspend fun run(params: Params): Result

    fun invoke(
        scope: CoroutineScope,
        params: Params,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
        onResult: (Result) -> Unit = {}
    ) {
        val job = scope.async(dispatcher) { run(params) }
        scope.launch(Dispatchers.Main) { onResult(job.await()) }
    }
}

abstract class UseCaseNoParams<Result> {
    protected abstract suspend fun run(): Result

    fun invoke(
        scope: CoroutineScope,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
        onResult: (Result) -> Unit = {}
    ) {
        val job = scope.async(dispatcher) { run() }
        scope.launch(Dispatchers.Main) { onResult(job.await()) }
    }
}