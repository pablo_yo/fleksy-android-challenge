package com.fleksy.challenge.domain.model.tvshow

import com.fleksy.challenge.domain.model.common.ImageUrl

data class TopRatedTvShowList(
    val totalResults: Int,
    val totalPages: Int,
    val tvShows: List<RatedTvShow>
) {

    data class RatedTvShow(
        val id: Int,
        val name: String,
        val image: ImageUrl,
        val vote: Double
    )
}