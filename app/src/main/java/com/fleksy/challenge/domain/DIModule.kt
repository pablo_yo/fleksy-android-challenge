package com.fleksy.challenge.domain

import com.fleksy.challenge.data.repository.ConfigRepositoryImpl
import com.fleksy.challenge.data.repository.TvShowRepositoryImpl
import com.fleksy.challenge.domain.repository.ConfigRepository
import com.fleksy.challenge.domain.repository.TvShowRepository
import com.fleksy.challenge.domain.usecase.GetDetailTvShowUseCase
import com.fleksy.challenge.domain.usecase.GetSimilarTvShowListUseCase
import com.fleksy.challenge.domain.usecase.GetTopRatedTvShowUseCase
import com.fleksy.challenge.domain.usecase.UpdateApiConfigurationUseCase
import org.koin.dsl.module

val repositoryModule = module {

    factory<ConfigRepository> { ConfigRepositoryImpl(get()) }
    factory<TvShowRepository> { TvShowRepositoryImpl(get(), get()) }

}

val useCaseModule = module {

    factory { GetDetailTvShowUseCase(get()) }
    factory { GetSimilarTvShowListUseCase(get()) }
    factory { GetTopRatedTvShowUseCase(get()) }
    factory { UpdateApiConfigurationUseCase(get()) }

}