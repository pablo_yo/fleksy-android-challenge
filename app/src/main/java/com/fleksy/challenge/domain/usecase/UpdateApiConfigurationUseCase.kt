package com.fleksy.challenge.domain.usecase

import com.fleksy.challenge.domain.common.UseCaseNoParams
import com.fleksy.challenge.domain.model.config.ApiConfig
import com.fleksy.challenge.domain.repository.ConfigRepository

class UpdateApiConfigurationUseCase(
    private val repository: ConfigRepository
) : UseCaseNoParams<ApiConfig?>() {

    override suspend fun run(): ApiConfig? {
        return repository.getApiConfig()
    }

}