package com.fleksy.challenge.domain.repository

import com.fleksy.challenge.domain.model.config.ApiConfig

interface ConfigRepository {

    suspend fun getApiConfig(): ApiConfig?

}