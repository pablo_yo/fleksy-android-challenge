package com.fleksy.challenge.domain.usecase

import com.fleksy.challenge.domain.common.UseCase
import com.fleksy.challenge.domain.model.tvshow.TvShowDetail
import com.fleksy.challenge.domain.repository.TvShowRepository

class GetDetailTvShowUseCase(
    private val repository: TvShowRepository
) : UseCase<Int, TvShowDetail?>() {

    override suspend fun run(params: Int): TvShowDetail? =
        repository.getDetail(params)

}