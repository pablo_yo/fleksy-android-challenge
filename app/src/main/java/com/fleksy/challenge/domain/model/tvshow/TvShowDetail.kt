package com.fleksy.challenge.domain.model.tvshow

import com.fleksy.challenge.domain.model.common.Company
import com.fleksy.challenge.domain.model.common.ImageUrl
import com.fleksy.challenge.domain.model.common.Person

data class TvShowDetail(
    val id: Int,
    val name: String,
    val image: ImageUrl,
    val bgImage: ImageUrl,
    val vote: Double,
    val firstAirDate: String?,
    val lastAirDate: String?,
    val genre: List<String>?,
    val createdBy: List<Person>,
    val overview: String?,
    val watchOn: List<Company>,
    val site: String?,
    val producedBy: List<Company>,
    val seasons: List<TvShowSeason>,
    val tagline: String?
)