package com.fleksy.challenge.domain.model.common

data class Company(
    val name: String,
    val image: ImageUrl
)
