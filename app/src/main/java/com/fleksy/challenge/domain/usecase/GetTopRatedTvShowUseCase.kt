package com.fleksy.challenge.domain.usecase

import androidx.paging.PagingData
import com.fleksy.challenge.domain.common.UseCaseNoParams
import com.fleksy.challenge.domain.model.tvshow.TopRatedTvShowList
import com.fleksy.challenge.domain.repository.TvShowRepository
import kotlinx.coroutines.flow.Flow

class GetTopRatedTvShowUseCase(
    private val repository: TvShowRepository
) : UseCaseNoParams<Flow<PagingData<TopRatedTvShowList.RatedTvShow>>>() {

    override suspend fun run(): Flow<PagingData<TopRatedTvShowList.RatedTvShow>> {
        return repository.getTopRatedTvShows()
    }

}