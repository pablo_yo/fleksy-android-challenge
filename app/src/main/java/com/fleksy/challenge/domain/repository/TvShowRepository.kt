package com.fleksy.challenge.domain.repository

import androidx.paging.PagingData
import com.fleksy.challenge.domain.model.tvshow.SimilarTvShow
import com.fleksy.challenge.domain.model.tvshow.TopRatedTvShowList
import com.fleksy.challenge.domain.model.tvshow.TvShowDetail
import kotlinx.coroutines.flow.Flow

interface TvShowRepository {

    suspend fun getTopRatedTvShows(): Flow<PagingData<TopRatedTvShowList.RatedTvShow>>

    suspend fun getDetail(tvId: Int): TvShowDetail?

    suspend fun getSimilarTvShows(tvId: Int, requestedPage: Int): List<SimilarTvShow>
}