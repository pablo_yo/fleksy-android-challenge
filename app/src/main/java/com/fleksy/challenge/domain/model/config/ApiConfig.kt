package com.fleksy.challenge.domain.model.config

data class ApiConfig(
    val baseImageUrl:String,
    val backdropSizes: List<String>,
    val posterSizes: List<String>,
)
