package com.fleksy.challenge.domain.usecase

import com.fleksy.challenge.domain.common.UseCase
import com.fleksy.challenge.domain.model.tvshow.SimilarTvShow
import com.fleksy.challenge.domain.repository.TvShowRepository

class GetSimilarTvShowListUseCase(
    private val repository: TvShowRepository
) : UseCase<GetSimilarTvShowListUseCase.Params, List<SimilarTvShow>>() {

    override suspend fun run(params: Params): List<SimilarTvShow> =
        repository.getSimilarTvShows(params.tvId, params.requestPage)

    data class Params(
        val tvId: Int,
        val requestPage: Int
    )

}