package com.fleksy.challenge.domain.model.common

data class Person(
    val name: String,
    val image: ImageUrl
)
