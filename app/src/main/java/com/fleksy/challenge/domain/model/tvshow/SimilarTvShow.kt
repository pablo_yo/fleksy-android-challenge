package com.fleksy.challenge.domain.model.tvshow

import com.fleksy.challenge.domain.model.common.ImageUrl

data class SimilarTvShow(
    val name: String,
    val image: ImageUrl,
    val bgImage: ImageUrl,
    val overview: String?,
    val vote: Double,
    val firstAirDate: String?
)