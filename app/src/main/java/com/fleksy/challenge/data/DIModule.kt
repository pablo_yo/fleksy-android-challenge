package com.fleksy.challenge.data

import com.fleksy.challenge.data.remote.configuration.api.ConfigApi
import com.fleksy.challenge.data.remote.configuration.datasource.ConfigRemoteDataSource
import com.fleksy.challenge.data.remote.configuration.datasource.ConfigRemoteDataSourceImpl
import com.fleksy.challenge.data.remote.tvshow.api.TvShowApi
import com.fleksy.challenge.data.remote.tvshow.datasource.TopRatedPagingDataSource
import com.fleksy.challenge.data.remote.tvshow.datasource.TvRemoteDataSource
import com.fleksy.challenge.data.remote.tvshow.datasource.TvRemoteDataSourceImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.themoviedb.org/3/" // TODO insert API Url

val appModule = module {

    /*** OkHTTP Instance ***/
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            })
            .retryOnConnectionFailure(true)
            .build()
    }

    /* Gson Instance */
    single {
        GsonBuilder().create()
    }

}

val networkModule = module {

    factory {
        provideApi<TvShowApi>(provideRetrofit(get(), get()))
    }
    factory {
        provideApi<ConfigApi>(provideRetrofit(get(), get()))
    }

}

val remoteModule = module {

    factory<ConfigRemoteDataSource> { ConfigRemoteDataSourceImpl(get()) }
    factory<TvRemoteDataSource> { TvRemoteDataSourceImpl(get()) }
    factory { TopRatedPagingDataSource(get()) }

}

private fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
}

inline fun <reified T> provideApi(retrofit: Retrofit): T = retrofit.create(T::class.java)
