package com.fleksy.challenge.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.data.mapper.toDomain
import com.fleksy.challenge.data.remote.tvshow.datasource.TopRatedPagingDataSource
import com.fleksy.challenge.data.remote.tvshow.datasource.TvRemoteDataSource
import com.fleksy.challenge.domain.model.tvshow.SimilarTvShow
import com.fleksy.challenge.domain.model.tvshow.TopRatedTvShowList
import com.fleksy.challenge.domain.model.tvshow.TvShowDetail
import com.fleksy.challenge.domain.repository.TvShowRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class TvShowRepositoryImpl(
    private val remote: TvRemoteDataSource,
    private val paging: TopRatedPagingDataSource
) : TvShowRepository {

    override suspend fun getTopRatedTvShows(): Flow<PagingData<TopRatedTvShowList.RatedTvShow>> {
        return Pager(
            config = PagingConfig(
                pageSize = ConstAndValues.NETWORK_PAGE_SIZE,
                prefetchDistance = 2
            ),
            initialKey = ConstAndValues.STARTING_PAGE_INDEX,
            pagingSourceFactory = { paging }
        ).flow.map {
            it.map { item -> item.toDomain() }
        }
    }

    override suspend fun getDetail(tvId: Int): TvShowDetail? {
        return remote.getDetail(tvId)
            .map { it.toDomain() }
            .getOrNull()
    }

    override suspend fun getSimilarTvShows(tvId: Int, requestedPage: Int): List<SimilarTvShow> {
        return remote.getSimilarTvShows(tvId, requestedPage)
            .map { list -> list.results?.map { it.toDomain() } }
            .getOrNull() ?: emptyList()
    }

}