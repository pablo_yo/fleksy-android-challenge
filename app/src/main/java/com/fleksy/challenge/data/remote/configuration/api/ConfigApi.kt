package com.fleksy.challenge.data.remote.configuration.api

import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.data.remote.configuration.model.ApiConfigDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ConfigApi {

    @GET("configuration")
    suspend fun getApiConfiguration(
        @Query(value = "api_key") apiKey: String = ConstAndValues.API_KEY
    ): Response<ApiConfigDto>

}