package com.fleksy.challenge.data.remote.tvshow.model

import com.google.gson.annotations.SerializedName

data class TvShowDetailDto(
    @SerializedName("backdrop_path")
    val backdropPath: String? = "",
    @SerializedName("created_by")
    val createdBy: List<CreatedByDto> = emptyList(),
    @SerializedName("episode_run_time")
    val episodeRunTime: List<Int> = emptyList(),
    @SerializedName("first_air_date")
    val firstAirDate: String? = "",
    val genres: List<GenreDto>? = emptyList(),
    val homepage: String? = "",
    val id: Int? = 0,
    @SerializedName("in_production")
    val inProduction: Boolean? = false,
    val languages: List<String> = emptyList(),
    @SerializedName("last_air_date")
    val lastAirDate: String? = "",
    @SerializedName("last_episode_to_air")
    val lastEpisodeToAir: LastEpisodeToAirDto? = LastEpisodeToAirDto(),
    val name: String? = "",
    val networks: List<NetworkDto> = emptyList(),
    @SerializedName("next_episode_to_air")
    val nextEpisodeToAir: Any? = Any(),
    @SerializedName("number_of_episodes")
    val numberOfEpisodes: Int? = 0,
    @SerializedName("number_of_seasons")
    val numberOfSeasons: Int? = 0,
    @SerializedName("origin_country")
    val originCountry: List<String> = emptyList(),
    @SerializedName("original_language")
    val originalLanguage: String? = "",
    @SerializedName("original_name")
    val originalName: String? = "",
    val overview: String? = "",
    val popularity: Double? = 0.0,
    @SerializedName("poster_path")
    val posterPath: String? = "",
    @SerializedName("production_companies")
    val productionCompanies: List<ProductionCompanyDto>? = emptyList(),
    @SerializedName("production_countries")
    val productionCountries: List<ProductionCountryDto> = emptyList(),
    val seasons: List<SeasonDto>? = emptyList(),
    @SerializedName("spoken_languages")
    val spokenLanguages: List<SpokenLanguageDto> = emptyList(),
    val status: String? = "",
    val tagline: String? = "",
    val type: String? = "",
    @SerializedName("vote_average")
    val voteAverage: Double? = 0.0,
    @SerializedName("vote_count")
    val voteCount: Int? = 0
) {

    data class CreatedByDto(
        @SerializedName("credit_id")
        val creditId: String? = "",
        val gender: Int? = 0,
        val id: Int? = 0,
        val name: String? = "",
        @SerializedName("profile_path")
        val profilePath: String? = ""
    )

    data class GenreDto(
        val id: Int? = 0,
        val name: String? = ""
    )

    data class LastEpisodeToAirDto(
        @SerializedName("air_date")
        val airDate: String? = "",
        @SerializedName("episode_number")
        val episodeNumber: Int? = 0,
        val id: Int? = 0,
        val name: String? = "",
        val overview: String? = "",
        @SerializedName("production_code")
        val productionCode: String? = "",
        @SerializedName("season_number")
        val seasonNumber: Int? = 0,
        @SerializedName("still_path")
        val stillPath: String? = "",
        @SerializedName("vote_average")
        val voteAverage: Double? = 0.0,
        @SerializedName("vote_count")
        val voteCount: Int? = 0
    )

    data class NetworkDto(
        val id: Int? = 0,
        @SerializedName("logo_path")
        val logoPath: String? = "",
        val name: String? = "",
        @SerializedName("origin_country")
        val originCountry: String? = ""
    )

    data class ProductionCompanyDto(
        val id: Int? = 0,
        @SerializedName("logo_path")
        val logoPath: String? = "",
        val name: String? = "",
        @SerializedName("origin_country")
        val originCountry: String? = ""
    )

    data class ProductionCountryDto(
        val iso_3166_1: String? = "",
        val name: String? = ""
    )

    data class SeasonDto(
        @SerializedName("air_date")
        val airDate: String? = "",
        @SerializedName("episode_count")
        val episodeCount: Int? = 0,
        val id: Int? = 0,
        val name: String? = "",
        val overview: String? = "",
        @SerializedName("poster_path")
        val posterPath: String? = "",
        @SerializedName("season_number")
        val seasonNumber: Int? = 0
    )

    data class SpokenLanguageDto(
        @SerializedName("english_name")
        val englishName: String? = "",
        val iso_639_1: String? = "",
        val name: String? = ""
    )
}