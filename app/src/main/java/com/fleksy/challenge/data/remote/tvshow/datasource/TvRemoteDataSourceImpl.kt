package com.fleksy.challenge.data.remote.tvshow.datasource

import com.fleksy.challenge.data.apiCall
import com.fleksy.challenge.data.remote.tvshow.api.TvShowApi
import com.fleksy.challenge.data.remote.tvshow.model.SimilarTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TvShowDetailDto

class TvRemoteDataSourceImpl(private val tvApi: TvShowApi) : TvRemoteDataSource {

    override suspend fun getDetail(tvId: Int): Result<TvShowDetailDto> {
        return apiCall { tvApi.getDetails(tvId) }
    }

    override suspend fun getSimilarTvShows(
        tvId: Int, requestedPage: Int
    ): Result<SimilarTvShowListDto> {
        return apiCall { tvApi.getSimilarTvShows(tvId = tvId, page = requestedPage) }
    }

}