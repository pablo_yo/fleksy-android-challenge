package com.fleksy.challenge.data.mapper

import com.fleksy.challenge.data.remote.configuration.model.ApiConfigDto
import com.fleksy.challenge.domain.model.config.ApiConfig

fun ApiConfigDto.toDomain() = ApiConfig(
    images?.baseUrl ?: "",
    images?.backdropSizes ?: emptyList(),
    images?.posterSizes ?: emptyList()
)