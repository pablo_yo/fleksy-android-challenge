package com.fleksy.challenge.data.mapper

import com.fleksy.challenge.data.remote.tvshow.model.SimilarTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TopRatedTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TvShowDetailDto
import com.fleksy.challenge.domain.model.common.Company
import com.fleksy.challenge.domain.model.common.ImageUrl
import com.fleksy.challenge.domain.model.common.Person
import com.fleksy.challenge.domain.model.tvshow.*

fun TopRatedTvShowListDto.RatedTvShowDto.toDomain() =
    TopRatedTvShowList.RatedTvShow(
        id ?: 0,
        name ?: "",
        ImageUrl(posterPath),
        voteAverage ?: 0.0
    )

fun TvShowDetailDto.toDomain() =
    TvShowDetail(
        id ?: 0,
        name ?: originalName ?: "",
        ImageUrl(posterPath),
        ImageUrl(backdropPath),
        voteAverage ?: 0.0,
        firstAirDate,
        lastAirDate,
        genres?.mapNotNull { it.name },
        createdBy.mapNotNull {
            Person(
                it.name ?: return@mapNotNull null,
                ImageUrl(it.profilePath ?: return@mapNotNull null)
            )
        },
        overview,
        networks.mapNotNull {
            Company(
                it.name ?: return@mapNotNull null,
                ImageUrl(it.logoPath ?: return@mapNotNull null)
            )
        },
        homepage,
        productionCompanies?.mapNotNull {
            Company(
                it.name ?: return@mapNotNull null,
                ImageUrl(it.logoPath ?: return@mapNotNull null)
            )
        } ?: emptyList(),
        seasons?.mapNotNull { season ->
            TvShowSeason(
                season.id ?: return@mapNotNull null,
                season.name ?: return@mapNotNull null,
                ImageUrl(season.posterPath ?: return@mapNotNull null),
                season.seasonNumber,
                season.episodeCount,
                season.airDate
            )
        } ?: emptyList(),
        tagline
    )

fun SimilarTvShowListDto.SimilarTvShowDto.toDomain() = SimilarTvShow(
    name ?: originalName ?: "",
    ImageUrl(posterPath),
    ImageUrl(backdropPath),
    overview,
    voteAverage ?: 0.0,
    firstAirDate
)