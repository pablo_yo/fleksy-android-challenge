package com.fleksy.challenge.data.remote.tvshow.model

import com.google.gson.annotations.SerializedName

data class TopRatedTvShowListDto(
    val page: Int? = 0,
    val results: List<RatedTvShowDto> = emptyList(),
    @SerializedName("total_pages")
    val totalPages: Int? = 0,
    @SerializedName("total_results")
    val totalResults: Int? = 0
) {

    data class RatedTvShowDto(
        @SerializedName("backdrop_path")
        val backdropPath: String? = "",
        @SerializedName("first_air_date")
        val firstAirDate: String? = "",
        @SerializedName("genre_ids")
        val genreIds: List<Int> = emptyList(),
        val id: Int? = 0,
        val name: String? = "",
        @SerializedName("origin_country")
        val originCountry: List<String> = emptyList(),
        @SerializedName("original_language")
        val originalLanguage: String? = "",
        @SerializedName("original_name")
        val originalName: String? = "",
        val overview: String? = "",
        val popularity: Double? = 0.0,
        @SerializedName("poster_path")
        val posterPath: String? = "",
        @SerializedName("vote_average")
        val voteAverage: Double? = 0.0,
        @SerializedName("total_pages")
        val vote_count: Int? = 0
    )
}