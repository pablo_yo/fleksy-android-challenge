package com.fleksy.challenge.data.remote.tvshow.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.data.apiCall
import com.fleksy.challenge.data.remote.tvshow.api.TvShowApi
import com.fleksy.challenge.data.remote.tvshow.model.TopRatedTvShowListDto

class TopRatedPagingDataSource(private val tvApi: TvShowApi) :
    PagingSource<Int, TopRatedTvShowListDto.RatedTvShowDto>() {

    override fun getRefreshKey(state: PagingState<Int, TopRatedTvShowListDto.RatedTvShowDto>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TopRatedTvShowListDto.RatedTvShowDto> {
        val pageNumber = params.key ?: ConstAndValues.STARTING_PAGE_INDEX
        return apiCall { tvApi.getTopRated(page = pageNumber) }
            .fold(
                onSuccess = {
                    LoadResult.Page(
                        data = it.results,
                        prevKey = if (pageNumber > ConstAndValues.STARTING_PAGE_INDEX)
                            pageNumber.minus(1) else null,
                        nextKey = if (it.results.isNotEmpty()) pageNumber.plus(1) else null
                    )
                },
                onFailure = { LoadResult.Error(it) }
            )
    }

}