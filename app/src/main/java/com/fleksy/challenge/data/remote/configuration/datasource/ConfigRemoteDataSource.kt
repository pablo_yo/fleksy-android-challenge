package com.fleksy.challenge.data.remote.configuration.datasource

import com.fleksy.challenge.data.remote.configuration.model.ApiConfigDto

interface ConfigRemoteDataSource {

    suspend fun getApiConfig(): Result<ApiConfigDto>

}