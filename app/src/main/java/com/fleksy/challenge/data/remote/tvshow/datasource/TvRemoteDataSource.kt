package com.fleksy.challenge.data.remote.tvshow.datasource

import com.fleksy.challenge.data.remote.tvshow.model.SimilarTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TvShowDetailDto

interface TvRemoteDataSource {

    suspend fun getDetail(tvId: Int): Result<TvShowDetailDto>

    suspend fun getSimilarTvShows(tvId: Int, requestedPage: Int): Result<SimilarTvShowListDto>

}