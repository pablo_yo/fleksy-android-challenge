package com.fleksy.challenge.data.remote.tvshow.api

import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.data.remote.tvshow.model.SimilarTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TopRatedTvShowListDto
import com.fleksy.challenge.data.remote.tvshow.model.TvShowDetailDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowApi {

    @GET("tv/top_rated")
    suspend fun getTopRated(
        @Query(value = "api_key") apiKey: String = ConstAndValues.API_KEY,
        @Query(value = "language") language: String = "en-US",
        @Query(value = "page") page: Int
    ): Response<TopRatedTvShowListDto>

    @GET("tv/{tv_id}")
    suspend fun getDetails(
        @Path(value = "tv_id") tvId: Int,
        @Query(value = "api_key") apiKey: String = ConstAndValues.API_KEY,
        @Query(value = "language") language: String = "en-US",
    ): Response<TvShowDetailDto>

    @GET("tv/{tv_id}/similar")
    suspend fun getSimilarTvShows(
        @Path(value = "tv_id") tvId: Int,
        @Query(value = "api_key") apiKey: String = ConstAndValues.API_KEY,
        @Query(value = "language") language: String = "en-US",
        @Query(value = "page") page: Int
    ): Response<SimilarTvShowListDto>

}