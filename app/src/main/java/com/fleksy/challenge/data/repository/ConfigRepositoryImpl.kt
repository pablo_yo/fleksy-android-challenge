package com.fleksy.challenge.data.repository

import com.fleksy.challenge.data.mapper.toDomain
import com.fleksy.challenge.data.remote.configuration.datasource.ConfigRemoteDataSource
import com.fleksy.challenge.domain.model.config.ApiConfig
import com.fleksy.challenge.domain.repository.ConfigRepository

class ConfigRepositoryImpl(private val remote: ConfigRemoteDataSource) : ConfigRepository {

    override suspend fun getApiConfig(): ApiConfig? {
        return remote.getApiConfig()
            .map { it.toDomain() }
            .getOrNull()
    }

}