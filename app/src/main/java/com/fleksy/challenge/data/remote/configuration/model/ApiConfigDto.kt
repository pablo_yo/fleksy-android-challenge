package com.fleksy.challenge.data.remote.configuration.model

import com.google.gson.annotations.SerializedName

data class ApiConfigDto(
    @SerializedName("change_keys")
    val changeKeys: List<String>? = emptyList(),
    val images: ImageConfig? = ImageConfig()
) {

    data class ImageConfig(
        @SerializedName("backdrop_sizes")
        val backdropSizes: List<String>? = emptyList(),
        @SerializedName("base_url")
        val baseUrl: String? = "",
        @SerializedName("logo_sizes")
        val logoSizes: List<String>? = emptyList(),
        @SerializedName("poster_sizes")
        val posterSizes: List<String>? = emptyList(),
        @SerializedName("profile_sizes")
        val profileSizes: List<String>? = emptyList(),
        @SerializedName("secure_base_url")
        val secureBaseUrl: String? = "",
        @SerializedName("still_sizes")
        val stillSizes: List<String>? = emptyList()
    )
}