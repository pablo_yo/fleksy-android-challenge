package com.fleksy.challenge.data.remote.configuration.datasource

import com.fleksy.challenge.data.apiCall
import com.fleksy.challenge.data.remote.configuration.api.ConfigApi
import com.fleksy.challenge.data.remote.configuration.model.ApiConfigDto

class ConfigRemoteDataSourceImpl(private val configApi: ConfigApi) : ConfigRemoteDataSource {

    override suspend fun getApiConfig(): Result<ApiConfigDto> {
        return apiCall { configApi.getApiConfiguration() }
    }

}