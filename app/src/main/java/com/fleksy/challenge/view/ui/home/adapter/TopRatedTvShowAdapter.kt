package com.fleksy.challenge.view.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.ItemRatedBinding
import com.fleksy.challenge.view.model.tvshow.RatedTvShowVO
import com.fleksy.challenge.view.ui.utils.OnRatedTvShowClickListener

class TopRatedTvShowAdapter(
    private val onClick: OnRatedTvShowClickListener
) : PagingDataAdapter<RatedTvShowVO, TopRatedTvShowAdapter.RatedItemViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatedItemViewHolder {
        return RatedItemViewHolder(
            ItemRatedBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RatedItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class RatedItemViewHolder(
        private val binding: ItemRatedBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: RatedTvShowVO) {
            binding.apply {
                itemPoster.apply {
                    transitionName = item.imageUri
                    load(item.imageUri)
                }

                itemTitle.text = item.name
                itemRatingText.text = item.vote.toString()
                itemRatingIndicator.progress = item.vote
                itemRatingIndicator.setIndicatorColor(
                    ContextCompat.getColor(
                        root.context, when {
                            item.vote < 25 -> R.color.red
                            item.vote < 50 -> R.color.orange
                            item.vote < 75 -> R.color.yellow
                            else -> R.color.green
                        }
                    )
                )

                root.setOnClickListener {
                    onClick(item, binding.itemPoster)
                }
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<RatedTvShowVO>() {
            override fun areItemsTheSame(oldItem: RatedTvShowVO, newItem: RatedTvShowVO) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: RatedTvShowVO, newItem: RatedTvShowVO) =
                oldItem == newItem
        }
    }

}