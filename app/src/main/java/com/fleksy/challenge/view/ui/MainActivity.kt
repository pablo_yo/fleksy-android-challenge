package com.fleksy.challenge.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fleksy.challenge.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}