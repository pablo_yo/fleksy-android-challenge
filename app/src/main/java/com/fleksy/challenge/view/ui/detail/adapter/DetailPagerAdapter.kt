package com.fleksy.challenge.view.ui.detail.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.ui.detail.intro.DetailIntroFragment
import com.fleksy.challenge.view.ui.detail.moreinfo.DetailMoreInfoFragment
import com.fleksy.challenge.view.ui.detail.season.DetailSeasonFragment

class DetailPagerAdapter(
    hostFragment: Fragment,
    private val detail: TvShowDetailVO
) : FragmentStateAdapter(hostFragment) {

    override fun getItemCount(): Int = if (detail.seasons.value.isNullOrEmpty()) 2 else 3

    override fun createFragment(position: Int): Fragment {
        return with(detail) {
            when (position) {
                0 -> DetailIntroFragment.newInstance(intro)
                1 -> DetailMoreInfoFragment.newInstance(moreInfo)
                else -> DetailSeasonFragment.newInstance(seasons)
            }
        }
    }

}