package com.fleksy.challenge.view.ui.similar.detail

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO

class SimilarDetailViewModel : ViewModel(), LifecycleObserver {

    private val _detail by lazy { MutableLiveData<SimilarTvShowVO>() }
    val detail: LiveData<SimilarTvShowVO>
        get() = _detail

    fun setSimilarDetail(similar: SimilarTvShowVO) {
        _detail.value = similar
    }

}