package com.fleksy.challenge.view.ui.home

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.domain.usecase.GetTopRatedTvShowUseCase
import com.fleksy.challenge.domain.usecase.UpdateApiConfigurationUseCase
import com.fleksy.challenge.view.mapper.map
import com.fleksy.challenge.view.model.tvshow.RatedTvShowVO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class HomeViewModel(
    private val updateApiConfig: UpdateApiConfigurationUseCase,
    private val getTopRatedFlow: GetTopRatedTvShowUseCase
) : ViewModel(), LifecycleObserver {

    private lateinit var _topRatedFlow: Flow<PagingData<RatedTvShowVO>>
    val topRatedFlow: Flow<PagingData<RatedTvShowVO>>
        get() = _topRatedFlow

    private val _flowReady by lazy { MutableLiveData<Boolean>() }
    val flowReady: LiveData<Boolean> = _flowReady

    init {
        updateConfiguration()
    }

    private fun updateConfiguration() {
        updateApiConfig.invoke(viewModelScope) {
            it?.let {
                ConstAndValues.apply {
                    baseImageUrl = it.baseImageUrl
                }
            }
        }
    }

    fun getTopRatedTvShows() {
        getTopRatedFlow.invoke(viewModelScope) { flow ->
            _topRatedFlow = flow.map { pagingData ->
                pagingData.map { item -> item.map() }
            }.cachedIn(viewModelScope)

            _flowReady.value = true
        }
    }

}