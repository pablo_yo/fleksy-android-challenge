package com.fleksy.challenge.view.mapper

import com.fleksy.challenge.domain.model.common.Company
import com.fleksy.challenge.domain.model.common.ImageType
import com.fleksy.challenge.domain.model.common.Person
import com.fleksy.challenge.domain.model.tvshow.SimilarTvShow
import com.fleksy.challenge.domain.model.tvshow.TopRatedTvShowList
import com.fleksy.challenge.domain.model.tvshow.TvShowDetail
import com.fleksy.challenge.domain.model.tvshow.TvShowSeason
import com.fleksy.challenge.view.model.common.CompanyVO
import com.fleksy.challenge.view.model.common.PersonVO
import com.fleksy.challenge.view.model.tvshow.RatedTvShowVO
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.model.tvshow.TvShowSeasonVO

fun TopRatedTvShowList.RatedTvShow.map() = RatedTvShowVO(
    id,
    name,
    image[ImageType.POSTER],
    (vote * 10).toInt()
)

fun TvShowDetail.map() =
    TvShowDetailVO(
        id,
        name,
        image[ImageType.POSTER],
        bgImage[ImageType.BACKGROUND],
        TvShowDetailVO.Intro(
            overview,
            firstAirDate,
            lastAirDate,
            genre?.joinToString(", "),
            (vote * 10).toInt(),
            site,
            tagline,
            createdBy.map { it.mapPerson() },
            producedBy.map { it.mapCompany() }
        ),
        TvShowDetailVO.MoreInfo(
            id,
            watchOn.map { it.mapCompany() },
        ),
        TvShowDetailVO.Seasons(
            seasons.map { it.mapSeason() }
        )
    )

private fun Person.mapPerson() = PersonVO(
    name, image[ImageType.PROFILE]
)

private fun Company.mapCompany() = CompanyVO(
    name, image[ImageType.LOGO]
)

private fun TvShowSeason.mapSeason() = TvShowSeasonVO(
    name, image[ImageType.POSTER], episodeCount, airDate
)

fun SimilarTvShow.map() =
    SimilarTvShowVO(
        name,
        image[ImageType.POSTER],
        bgImage[ImageType.BACKGROUND],
        overview,
        (vote * 10).toInt(),
        firstAirDate
    )
