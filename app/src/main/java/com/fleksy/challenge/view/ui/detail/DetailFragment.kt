package com.fleksy.challenge.view.ui.detail

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import coil.load
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.DetailFragmentBinding
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.ui.detail.adapter.DetailPagerAdapter
import com.fleksy.challenge.view.ui.utils.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : BaseFragment<DetailFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DetailFragmentBinding =
        DetailFragmentBinding::inflate

    override val viewModel: DetailViewModel by viewModel()

    private val args: DetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(TransitionInflater.from(context).inflateTransition(android.R.transition.move)) {
            sharedElementEnterTransition = this
            sharedElementReturnTransition = this
        }
    }

    override fun onFragmentReady() {
        binding.apply {
            poster.apply {
                transitionName = args.imageUri
                load(args.imageUri)
            }
        }
        viewModel.apply {
            tvShow.observe(viewLifecycleOwner) {
                handleTvShowDetail(it)
            }

            getDetail(args.tvId)
        }
    }

    private fun handleTvShowDetail(detail: TvShowDetailVO) {
        binding.apply {
            poster.load(detail.image)
            background.load(detail.bgImage)
            title.text = detail.name
            pager.adapter = DetailPagerAdapter(this@DetailFragment, detail)

            val tabLabels = resources.getStringArray(R.array.detail_tabs)
            TabLayoutMediator(tabLayout, pager) { tab, position ->
                tab.text = tabLabels[position]
            }.attach()
        }
    }

}