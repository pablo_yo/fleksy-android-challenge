package com.fleksy.challenge.view.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.fleksy.challenge.databinding.ItemSimilarBinding
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.ui.utils.OnSimilarTvShowClickListener

class DetailSimilarTvShowsAdapter(
    val onItemClick: OnSimilarTvShowClickListener
) : ListAdapter<SimilarTvShowVO, DetailSimilarTvShowsAdapter.SimilarItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarItemViewHolder {
        return SimilarItemViewHolder(
            ItemSimilarBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SimilarItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class SimilarItemViewHolder(
        private val binding: ItemSimilarBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SimilarTvShowVO) {
            binding.apply {
                itemPoster.load(item.image)
                itemTitle.text = item.name

                root.setOnClickListener {
                    onItemClick(bindingAdapterPosition)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<SimilarTvShowVO>() {
            override fun areItemsTheSame(oldItem: SimilarTvShowVO, newItem: SimilarTvShowVO) =
                oldItem.image == newItem.image

            override fun areContentsTheSame(oldItem: SimilarTvShowVO, newItem: SimilarTvShowVO) =
                oldItem == newItem
        }
    }

}