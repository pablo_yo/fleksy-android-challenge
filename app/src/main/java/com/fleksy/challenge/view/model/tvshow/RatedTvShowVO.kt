package com.fleksy.challenge.view.model.tvshow

data class RatedTvShowVO(
    val id: Int,
    val name: String,
    val imageUri: String,
    val vote: Int
)