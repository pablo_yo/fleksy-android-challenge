package com.fleksy.challenge.view.model.common

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CompanyVO(
    val name: String,
    val logo: String
) : Parcelable