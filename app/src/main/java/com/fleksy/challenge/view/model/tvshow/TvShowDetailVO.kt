package com.fleksy.challenge.view.model.tvshow

import android.os.Parcelable
import com.fleksy.challenge.view.model.common.CompanyVO
import com.fleksy.challenge.view.model.common.PersonVO
import kotlinx.parcelize.Parcelize

data class TvShowDetailVO(
    val id: Int,
    val name: String,
    val image: String,
    val bgImage: String,
    val intro: Intro,
    val moreInfo: MoreInfo,
    val seasons: Seasons,
) {

    @Parcelize
    data class Intro(
        val overview: String?,
        val firstAirDate: String?,
        val lastAirDate: String?,
        val genres: String?,
        val vote: Int,
        val site: String?,
        val tagline: String?,
        val createdBy: List<PersonVO>,
        val producedBy: List<CompanyVO>,
    ) : Parcelable

    @Parcelize
    data class Seasons(
        val value: List<TvShowSeasonVO>
    ) : Parcelable

    @Parcelize
    data class MoreInfo(
        val tvShowId: Int,
        val watchOn: List<CompanyVO>,
    ) : Parcelable

}
