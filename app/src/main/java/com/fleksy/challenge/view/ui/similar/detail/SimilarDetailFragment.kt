package com.fleksy.challenge.view.ui.similar.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import coil.load
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.SimilarDetailFragmentBinding
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.text
import com.fleksy.challenge.view.ui.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SimilarDetailFragment : BaseFragment<SimilarDetailFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> SimilarDetailFragmentBinding =
        SimilarDetailFragmentBinding::inflate

    override val viewModel: SimilarDetailViewModel by viewModel()

    override fun getBundleArguments(arguments: Bundle) {
        arguments.getParcelable<SimilarTvShowVO>(SIMILAR_DETAIL)?.let {
            viewModel.setSimilarDetail(it)
        }
    }

    override fun onFragmentReady() {
        viewModel.apply {
            detail.observe(viewLifecycleOwner) {
                handleSimilarDetail(it)
            }
        }
    }

    private fun handleSimilarDetail(detail: SimilarTvShowVO) {
        binding.apply {
            background.load(detail.bgImage)
            title.text = detail.name
            ratingText.text = detail.vote.toString()
            ratingIndicator.apply {
                progress = detail.vote
                setIndicatorColor(
                    ContextCompat.getColor(
                        root.context, when {
                            detail.vote < 25 -> R.color.red
                            detail.vote < 50 -> R.color.orange
                            detail.vote < 75 -> R.color.yellow
                            else -> R.color.green
                        }
                    )
                )
            }
            poster.load(detail.image)
            overview.text(detail.overview)
            release.text(R.string.release_date, detail.airDate)
        }
    }

    companion object {
        const val SIMILAR_DETAIL = "similarItem"

        fun newInstance(similar: SimilarTvShowVO) =
            SimilarDetailFragment().apply {
                arguments = bundleOf(
                    SIMILAR_DETAIL to similar
                )
            }
    }

}