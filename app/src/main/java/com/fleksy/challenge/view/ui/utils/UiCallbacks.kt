package com.fleksy.challenge.view.ui.utils

import android.widget.ImageView
import com.fleksy.challenge.view.model.tvshow.RatedTvShowVO

typealias OnRatedTvShowClickListener = (item: RatedTvShowVO, imageView: ImageView) -> Unit

typealias OnSimilarTvShowClickListener = (position: Int) -> Unit