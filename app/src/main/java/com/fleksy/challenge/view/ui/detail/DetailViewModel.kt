package com.fleksy.challenge.view.ui.detail

import androidx.lifecycle.*
import com.fleksy.challenge.domain.usecase.GetDetailTvShowUseCase
import com.fleksy.challenge.view.mapper.map
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO

class DetailViewModel(
    private val detailUseCase: GetDetailTvShowUseCase
) : ViewModel(), LifecycleObserver {

    private val _tvShow by lazy { MutableLiveData<TvShowDetailVO>() }
    val tvShow: LiveData<TvShowDetailVO>
        get() = _tvShow

    fun getDetail(idTv: Int) {
        detailUseCase.invoke(viewModelScope, idTv) {
            it?.let { _tvShow.value = it.map() }
        }
    }

}