package com.fleksy.challenge.view.model.common

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PersonVO(
    val name: String,
    val image: String
) : Parcelable