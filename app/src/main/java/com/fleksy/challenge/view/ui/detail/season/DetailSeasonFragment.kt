package com.fleksy.challenge.view.ui.detail.season

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.fleksy.challenge.databinding.DetailSeasonFragmentBinding
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.model.tvshow.TvShowSeasonVO
import com.fleksy.challenge.view.ui.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailSeasonFragment : BaseFragment<DetailSeasonFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DetailSeasonFragmentBinding =
        DetailSeasonFragmentBinding::inflate

    override val viewModel: DetailSeasonViewModel by viewModel()

    override fun getBundleArguments(arguments: Bundle) {
        (arguments.getParcelable<TvShowDetailVO.Seasons>(DETAIL_SEASONS))?.let {
            viewModel.setSeasons(it)
        }
    }

    override fun onFragmentReady() {
        binding.seasonsList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = DetailSeasonAdapter()
        }
        viewModel.apply {
            seasons.observe(viewLifecycleOwner) {
                handleSeasonList(it)
            }
        }
    }

    private fun handleSeasonList(seasons: List<TvShowSeasonVO>) {
        (binding.seasonsList.adapter as DetailSeasonAdapter).submitList(seasons)
    }

    companion object {
        private const val DETAIL_SEASONS = "detailSeasons"

        fun newInstance(seasons: TvShowDetailVO.Seasons) =
            DetailSeasonFragment().apply {
                arguments = bundleOf(DETAIL_SEASONS to seasons)
            }
    }

}