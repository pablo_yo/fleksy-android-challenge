package com.fleksy.challenge.view.ui.similar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.fleksy.challenge.databinding.SimilarFragmentBinding
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.ui.similar.adapter.SimilarPagerAdapter
import com.fleksy.challenge.view.ui.similar.component.ZoomOutPageTransformer
import com.fleksy.challenge.view.ui.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SimilarFragment : BaseFragment<SimilarFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> SimilarFragmentBinding =
        SimilarFragmentBinding::inflate

    override val viewModel: SimilarViewModel by viewModel()

    override fun getBundleArguments(arguments: Bundle) {
        arguments.apply {
            viewModel.selectedIndex = getInt(SELECTED_INDEX, 0)
            getParcelableArrayList<SimilarTvShowVO>(SIMILAR_ITEMS)?.let {
                viewModel.setSimilarTvShows(it)
            }
        }
    }

    override fun onFragmentReady() {
        binding.similarPager.setPageTransformer(ZoomOutPageTransformer())
        viewModel.apply {
            similarTvShows.observe(viewLifecycleOwner) {
                handleSimilarTvShows(it)
            }
        }
    }

    private fun handleSimilarTvShows(similars: List<SimilarTvShowVO>) {
        binding.similarPager.apply {
            adapter = SimilarPagerAdapter(this@SimilarFragment, similars)
            currentItem = viewModel.selectedIndex
        }
    }

    companion object {
        const val SELECTED_INDEX = "selectedIndex"
        const val SIMILAR_ITEMS = "similarItems"
    }

}