package com.fleksy.challenge.view.model.tvshow

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SimilarTvShowVO(
    val name: String,
    val image: String,
    val bgImage: String,
    val overview: String?,
    val vote: Int,
    val airDate: String?
) : Parcelable
