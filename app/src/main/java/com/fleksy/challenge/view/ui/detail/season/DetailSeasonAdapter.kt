package com.fleksy.challenge.view.ui.detail.season

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.ItemTvshowSeasonBinding
import com.fleksy.challenge.view.model.tvshow.TvShowSeasonVO
import com.fleksy.challenge.view.text

class DetailSeasonAdapter :
    ListAdapter<TvShowSeasonVO, DetailSeasonAdapter.SeasonItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonItemViewHolder {
        return SeasonItemViewHolder(
            ItemTvshowSeasonBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SeasonItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class SeasonItemViewHolder(
        private val binding: ItemTvshowSeasonBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TvShowSeasonVO) {
            binding.apply {
                itemPoster.load(item.image)
                itemTitle.text = item.name
                itemEpisodes.text(R.string.number_of_episodes, item.episodeCount)
                itemRelease.text(R.string.release_date, item.airDate)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TvShowSeasonVO>() {
            override fun areItemsTheSame(oldItem: TvShowSeasonVO, newItem: TvShowSeasonVO) =
                oldItem.image == newItem.image

            override fun areContentsTheSame(oldItem: TvShowSeasonVO, newItem: TvShowSeasonVO) =
                oldItem == newItem
        }
    }

}