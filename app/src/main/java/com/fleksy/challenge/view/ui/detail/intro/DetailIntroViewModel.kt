package com.fleksy.challenge.view.ui.detail.intro

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO.Intro

class DetailIntroViewModel : ViewModel(), LifecycleObserver {

    private val _introInfo by lazy { MutableLiveData<Intro>() }
    val introInfo: LiveData<Intro>
        get() = _introInfo

    fun setIntroInfo(intro: Intro) {
        _introInfo.value = intro
    }

}