package com.fleksy.challenge.view.ui.detail.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.DetailIntroFragmentBinding
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.text
import com.fleksy.challenge.view.ui.detail.adapter.DetailCompanyAdapter
import com.fleksy.challenge.view.ui.detail.adapter.DetailPersonAdapter
import com.fleksy.challenge.view.ui.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailIntroFragment : BaseFragment<DetailIntroFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DetailIntroFragmentBinding =
        DetailIntroFragmentBinding::inflate

    override val viewModel: DetailIntroViewModel by viewModel()

    override fun getBundleArguments(arguments: Bundle) {
        (arguments.getParcelable<TvShowDetailVO.Intro>(DETAIL_INTRO))?.let {
            viewModel.setIntroInfo(it)
        }
    }

    override fun onFragmentReady() {
        binding.apply {
            createdByList.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = DetailPersonAdapter()
            }
            producedByList.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = DetailCompanyAdapter()
            }
        }
        viewModel.apply {
            introInfo.observe(viewLifecycleOwner) {
                handleIntroInfo(it)
            }
        }
    }

    private fun handleIntroInfo(intro: TvShowDetailVO.Intro) {
        binding.apply {
            ratingText.text = intro.vote.toString()
            ratingIndicator.apply {
                progress = intro.vote
                setIndicatorColor(
                    ContextCompat.getColor(
                        root.context, when {
                            intro.vote < 25 -> R.color.red
                            intro.vote < 50 -> R.color.orange
                            intro.vote < 75 -> R.color.yellow
                            else -> R.color.green
                        }
                    )
                )
            }

            tagline.text(intro.tagline)
            genres.text(intro.genres)
            firstEmissionDate.text(R.string.first_emission_label, intro.firstAirDate)
            lastEmissionDate.text(R.string.last_emission_label, intro.lastAirDate)
            overview.text(intro.overview)

            createdByContainer.isVisible = !intro.createdBy.isNullOrEmpty()
            (createdByList.adapter as DetailPersonAdapter).submitList(intro.createdBy)

            producedByContainer.isVisible = !intro.producedBy.isNullOrEmpty()
            (producedByList.adapter as DetailCompanyAdapter).submitList(intro.producedBy)
        }
    }

    companion object {
        private const val DETAIL_INTRO = "detailIntroInfo"

        fun newInstance(intro: TvShowDetailVO.Intro) =
            DetailIntroFragment().apply {
                arguments = bundleOf(DETAIL_INTRO to intro)
            }
    }

}