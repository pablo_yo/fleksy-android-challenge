package com.fleksy.challenge.view.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.fleksy.challenge.databinding.ItemCompanyBinding
import com.fleksy.challenge.view.model.common.CompanyVO

class DetailCompanyAdapter :
    ListAdapter<CompanyVO, DetailCompanyAdapter.CompanyItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyItemViewHolder {
        return CompanyItemViewHolder(
            ItemCompanyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CompanyItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class CompanyItemViewHolder(
        private val binding: ItemCompanyBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CompanyVO) {
            binding.apply {
                itemImage.load(item.logo)
                itemTitle.text = item.name
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CompanyVO>() {
            override fun areItemsTheSame(oldItem: CompanyVO, newItem: CompanyVO) =
                oldItem.logo == newItem.logo

            override fun areContentsTheSame(oldItem: CompanyVO, newItem: CompanyVO) =
                oldItem == newItem
        }
    }

}