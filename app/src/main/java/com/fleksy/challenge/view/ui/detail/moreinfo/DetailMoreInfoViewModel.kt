package com.fleksy.challenge.view.ui.detail.moreinfo

import androidx.lifecycle.*
import com.fleksy.challenge.ConstAndValues
import com.fleksy.challenge.domain.usecase.GetSimilarTvShowListUseCase
import com.fleksy.challenge.view.mapper.map
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO.MoreInfo

class DetailMoreInfoViewModel(
    private val getSimilar: GetSimilarTvShowListUseCase
) : ViewModel(), LifecycleObserver {

    private val _moreInfo by lazy { MutableLiveData<MoreInfo>() }
    val moreInfo: LiveData<MoreInfo>
        get() = _moreInfo

    private val _similarTvShows by lazy { MutableLiveData<List<SimilarTvShowVO>>() }
    val similarTvShows: LiveData<List<SimilarTvShowVO>>
        get() = _similarTvShows

    fun setMoreInfo(moreInfo: MoreInfo) {
        _moreInfo.value = moreInfo
    }

    fun getSimilarTvShows(tvShowId: Int) {
        getSimilar.invoke(
            viewModelScope,
            GetSimilarTvShowListUseCase.Params(tvShowId, ConstAndValues.STARTING_PAGE_INDEX)
        ) {
            _similarTvShows.value = it.map { similar -> similar.map() }
        }
    }

}