package com.fleksy.challenge.view.model.tvshow

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TvShowSeasonVO(
    val name: String,
    val image: String,
    val episodeCount: Int?,
    val airDate: String?
) : Parcelable
