package com.fleksy.challenge.view.ui.detail.season

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO.Seasons
import com.fleksy.challenge.view.model.tvshow.TvShowSeasonVO

class DetailSeasonViewModel : ViewModel(), LifecycleObserver {

    private val _seasons by lazy { MutableLiveData<List<TvShowSeasonVO>>() }
    val seasons: LiveData<List<TvShowSeasonVO>>
        get() = _seasons

    fun setSeasons(seasons: Seasons) {
        _seasons.value = seasons.value
    }

}