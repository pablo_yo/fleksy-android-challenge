package com.fleksy.challenge.view

import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<in T>) {
    observe(lifecycleOwner, object : Observer<T> {
        override fun onChanged(t: T?) {
            observer.onChanged(t)
            removeObserver(this)
        }
    })
}

fun TextView.text(text: String?) {
    isVisible = !text.isNullOrEmpty()
    text?.let { this.text = it }
}

fun TextView.text(@StringRes resId: Int, text: String?) {
    isVisible = !text.isNullOrEmpty()
    text?.let { this.text = resources.getString(resId, text) }
}

fun TextView.text(@StringRes resId: Int, value: Int?) {
    isVisible = value != null
    value?.let { this.text = resources.getString(resId, value) }
}