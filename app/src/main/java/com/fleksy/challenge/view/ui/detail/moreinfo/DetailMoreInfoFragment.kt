package com.fleksy.challenge.view.ui.detail.moreinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fleksy.challenge.R
import com.fleksy.challenge.databinding.DetailMoreInfoFragmentBinding
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.model.tvshow.TvShowDetailVO
import com.fleksy.challenge.view.ui.detail.adapter.DetailCompanyAdapter
import com.fleksy.challenge.view.ui.detail.adapter.DetailSimilarTvShowsAdapter
import com.fleksy.challenge.view.ui.similar.SimilarFragment.Companion.SELECTED_INDEX
import com.fleksy.challenge.view.ui.similar.SimilarFragment.Companion.SIMILAR_ITEMS
import com.fleksy.challenge.view.ui.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailMoreInfoFragment : BaseFragment<DetailMoreInfoFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DetailMoreInfoFragmentBinding =
        DetailMoreInfoFragmentBinding::inflate

    override val viewModel: DetailMoreInfoViewModel by viewModel()

    override fun getBundleArguments(arguments: Bundle) {
        (arguments.getParcelable<TvShowDetailVO.MoreInfo>(DETAIL_MORE_INFO))?.let {
            viewModel.apply {
                setMoreInfo(it)
                getSimilarTvShows(it.tvShowId)
            }
        }
    }

    override fun onFragmentReady() {
        binding.apply {
            watchOnList.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = DetailCompanyAdapter()
            }
            similarList.apply {
                layoutManager = GridLayoutManager(context, NUM_COLUMNS)
                adapter = DetailSimilarTvShowsAdapter {
                    onSimilarItemClick(it)
                }.apply {
                    stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
                }
            }
        }
        viewModel.apply {
            moreInfo.observe(viewLifecycleOwner) {
                handleMoreInfo(it)
            }
            similarTvShows.observe(viewLifecycleOwner) {
                hadleSimilarTvSHows(it)
            }
        }
    }

    private fun handleMoreInfo(moreInfo: TvShowDetailVO.MoreInfo) {
        binding.apply {
            watchOnContainer.isVisible = !moreInfo.watchOn.isNullOrEmpty()
            (watchOnList.adapter as DetailCompanyAdapter).submitList(moreInfo.watchOn)
        }
    }

    private fun hadleSimilarTvSHows(similars: List<SimilarTvShowVO>) {
        binding.apply {
            similarContainer.isVisible = !similars.isNullOrEmpty()
            (similarList.adapter as DetailSimilarTvShowsAdapter).submitList(similars)
        }
    }

    private fun onSimilarItemClick(position: Int) {
        parentFragment?.findNavController()?.navigate(
            R.id.actionSimilarList,
            bundleOf(
                SELECTED_INDEX to position,
                SIMILAR_ITEMS to (viewModel.similarTvShows.value ?: emptyList())
            )
        )
    }

    companion object {
        private const val NUM_COLUMNS = 3
        private const val DETAIL_MORE_INFO = "detailMoreInfo"

        fun newInstance(moreInfo: TvShowDetailVO.MoreInfo) =
            DetailMoreInfoFragment().apply {
                arguments = bundleOf(
                    DETAIL_MORE_INFO to moreInfo
                )
            }
    }

}