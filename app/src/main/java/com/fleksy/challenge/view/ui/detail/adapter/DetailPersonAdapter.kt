package com.fleksy.challenge.view.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.fleksy.challenge.databinding.ItemPersonBinding
import com.fleksy.challenge.view.model.common.PersonVO

class DetailPersonAdapter :
    ListAdapter<PersonVO, DetailPersonAdapter.PersonItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonItemViewHolder {
        return PersonItemViewHolder(
            ItemPersonBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PersonItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class PersonItemViewHolder(
        private val binding: ItemPersonBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PersonVO) {
            binding.apply {
                itemImage.load(item.image)
                itemTitle.text = item.name
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PersonVO>() {
            override fun areItemsTheSame(oldItem: PersonVO, newItem: PersonVO) =
                oldItem.image == newItem.image

            override fun areContentsTheSame(oldItem: PersonVO, newItem: PersonVO) =
                oldItem == newItem
        }
    }

}