package com.fleksy.challenge.view.ui.similar

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO

class SimilarViewModel : ViewModel(), LifecycleObserver {

    private val _similarTvShows by lazy { MutableLiveData<List<SimilarTvShowVO>>() }
    val similarTvShows: LiveData<List<SimilarTvShowVO>>
        get() = _similarTvShows

    var selectedIndex: Int = 0

    fun setSimilarTvShows(similars: List<SimilarTvShowVO>) {
        _similarTvShows.value = similars
    }

}