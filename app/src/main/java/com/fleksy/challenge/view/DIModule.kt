package com.fleksy.challenge.view

import com.fleksy.challenge.view.ui.detail.DetailViewModel
import com.fleksy.challenge.view.ui.detail.intro.DetailIntroViewModel
import com.fleksy.challenge.view.ui.detail.moreinfo.DetailMoreInfoViewModel
import com.fleksy.challenge.view.ui.detail.season.DetailSeasonViewModel
import com.fleksy.challenge.view.ui.home.HomeViewModel
import com.fleksy.challenge.view.ui.similar.detail.SimilarDetailViewModel
import com.fleksy.challenge.view.ui.similar.SimilarViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    // Home view
    viewModel { HomeViewModel(get(), get()) }

    // Detail view
    viewModel { DetailViewModel(get()) }
    viewModel { DetailIntroViewModel() }
    viewModel { DetailSeasonViewModel() }
    viewModel { DetailMoreInfoViewModel(get()) }

    // Similar contents view
    viewModel { SimilarViewModel() }
    viewModel { SimilarDetailViewModel() }

}