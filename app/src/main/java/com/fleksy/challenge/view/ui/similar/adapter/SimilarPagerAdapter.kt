package com.fleksy.challenge.view.ui.similar.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fleksy.challenge.view.model.tvshow.SimilarTvShowVO
import com.fleksy.challenge.view.ui.similar.detail.SimilarDetailFragment

class SimilarPagerAdapter(
    hostFragment: Fragment,
    val items: List<SimilarTvShowVO>
) : FragmentStateAdapter(hostFragment) {

    override fun getItemCount(): Int = items.size

    override fun createFragment(position: Int): Fragment =
        SimilarDetailFragment.newInstance(items[position])
}