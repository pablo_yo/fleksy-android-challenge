package com.fleksy.challenge.view.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fleksy.challenge.databinding.HomeFragmentBinding
import com.fleksy.challenge.view.observeOnce
import com.fleksy.challenge.view.ui.home.adapter.TopRatedTvShowAdapter
import com.fleksy.challenge.view.ui.utils.BaseFragment
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeFragmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> HomeFragmentBinding =
        HomeFragmentBinding::inflate

    override val viewModel: HomeViewModel by viewModel()

    override fun onFragmentReady() {
        // Manage transition from detail fragment
        postponeEnterTransition()

        binding.apply {
            topRatedList.apply {
                layoutManager = GridLayoutManager(context, NUM_COLUMNS)
                adapter = TopRatedTvShowAdapter { item, imageView ->
                    findNavController().navigate(
                        HomeFragmentDirections.actionOpenDetail(item.id, item.imageUri),
                        FragmentNavigatorExtras(
                            imageView to item.imageUri
                        )
                    )
                }.apply {
                    stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
                }

                // Load image transition from detail fragment
                post { startPostponedEnterTransition() }
            }
        }

        viewModel.apply {
            flowReady.observeOnce(viewLifecycleOwner) { ready ->
                if (ready) {
                    viewLifecycleOwner.lifecycleScope.launchWhenCreated {
                        topRatedFlow.collectLatest {
                            (binding.topRatedList.adapter as TopRatedTvShowAdapter).submitData(it)
                        }
                    }
                }
            }

            getTopRatedTvShows()
        }

    }

    companion object {
        private const val NUM_COLUMNS = 2
    }

}