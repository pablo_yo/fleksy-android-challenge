package com.fleksy.challenge

import android.app.Application
import com.fleksy.challenge.data.appModule
import com.fleksy.challenge.data.networkModule
import com.fleksy.challenge.data.remoteModule
import com.fleksy.challenge.domain.repositoryModule
import com.fleksy.challenge.domain.useCaseModule
import com.fleksy.challenge.view.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class FleksyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@FleksyApplication)
            // module list
            val moduleList = listOf(
                appModule,
                networkModule,
                remoteModule,
                repositoryModule,
                useCaseModule,
                viewModelModule
            )
            loadKoinModules(moduleList)
        }
    }

}